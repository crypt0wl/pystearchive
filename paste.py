#!/usr/bin/env python3

import requests
import logging

'''
Defines what attribute every paste stored locally needs to adhere to and
standardizes interaction with the data.
'''

from datetime import datetime, timezone

class Paste:
    def __init__(self, key, date, size, expire, title, user):
        self.key = key
        self.date = datetime.fromtimestamp(date, tz=timezone.utc)
        self.size = size
        self.expire = expire
        self.title = title
        self.user = user

    def get_raw_content(self):
        try:
            self.content = requests.get("https://pastebin.com/raw/" + self.key).text
            return self.content
        except (ConnectionError, HTTPError, Timeout) as con_err:
            return con_err

