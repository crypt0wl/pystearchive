### pystearchive

pystearchive is a tool for fetching, storing and presenting (through an API)
public pastes from [pastebin.com](https://pastebin.com)

>**Important**: This isn't a stable tool by any definition, I'm tinkering
>around with it constantly. There's not guarantee that anything works, even if
>it's in the main branch.

#### Requirements

The required third-party modules are listed in `requirements.txt`:

`pip install -r requirements.txt`

Since the tool relies on Pastebins [Scraping
API](https://pastebin.com/doc_scraping_api) you need a [Pastebin Pro
account](https://pastebin.com/pro), otherwise it won't work.

#### Usage

#### Todo

Implement scraping from the following sites:

* [cl1p.net](https://cl1p.net/sys/api.jsp)
* [dpaste](https://dpaste.com/api/v2/)
* [gist.github.com](https://gist.github.com)
* [hastebin](https://hastebin.com/about.md)
* [paste.org.ru](http://paste.org.ru/?script) (doesn't provide an API, but a
  scrape-able format)
* [ideone](https://ideone.com/) (same here)

#### Credits

Inspired by [PasteHunter](https://github.com/kevthehermit/PasteHunter),
[EMAGNET](https://github.com/wuseman/EMAGNET),
[Scavenger](https://github.com/rndinfosecguy/Scavenger),
[pwnbin](https://github.com/kahunalu/pwnbin) and a lot of great other tools
that provde to be too big for my needs. The rights for the icon belong to
[Tkgd2007](https://commons.wikimedia.org/wiki/File:TK_archive_icon.svg).
